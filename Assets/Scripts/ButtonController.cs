﻿using Assets.Scripts.com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    [SerializeField]
    private Button _playButton;

    private void Start()
    {
        _playButton.onClick.AddListener(PlayTap);
    }
    private void PlayTap()
    {
        EventManager._PLAY(new EventData(true));
    }

}
