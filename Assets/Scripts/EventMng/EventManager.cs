﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.com
{
    public class EventManager
    {
        //Pressing "Play"
        public delegate void PLAYCLICK(IData data);
        //Getting coins;
        public delegate void ADDCOINS(IData data);
        //Getting a breakpoint
        public delegate void POINTSTOPED(IData data);

        static public PLAYCLICK _PLAY;
        static public ADDCOINS _ADDCOINS;
        static public POINTSTOPED _POINTSTOPED;
    }
}
