﻿using Assets.Scripts.com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMove : MonoBehaviour
{
    [SerializeField]
    private Transform _targer;
    [SerializeField]
    private float _speed;

    private Transform _current;
    private float _progress;

    private const string _COINSTATS = "coinStats";
    private void Start()
    {
        _current = transform;
    }
    private void Update()
    {
        transform.position = Vector3.Lerp(_current.position, _targer.position, _progress);
        _progress += _speed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Remove prefab and sending an event to add coins
        if (collision.gameObject.tag == _COINSTATS)
        {
            EventManager._ADDCOINS(new EventData(true));
            collision.gameObject.GetComponent<Animation>().Play();
            Destroy(gameObject);
        }
    }

}
