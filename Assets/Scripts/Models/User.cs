﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class User
    {
        public string name;
        public int progress;
        public int coins;
        public int chip;

        public User(string name, int progress, int coins, int chip)
        {
            this.name = name;
            this.progress = progress;
            this.coins = coins;
            this.chip = chip;
        }
    }
}
