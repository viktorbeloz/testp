﻿using Assets.Scripts.com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemColController : MonoBehaviour
{
    [SerializeField]
    private GameObject _coinPrefab;
    [SerializeField]
    private int _scoreItem;
    [SerializeField]
    private Slider _progressSlider;
    private bool _triger;
    private Animation _itemAnim;

    private void Start()
    {
        _itemAnim = gameObject.GetComponent<Animation>();
    }

    private void Update()
    {
        if (_scoreItem == (int)_progressSlider.value && !_triger)
        {
            _triger = true;
            StartCoroutine(PointCoroutine());
        }
    }
    //Stop notification and animation start
    private IEnumerator PointCoroutine()
    {
        EventManager._POINTSTOPED(new EventData(true));
        _itemAnim.Play();
        yield return new WaitForSeconds(1.5f);
        if(gameObject.tag!="rank")
        {
            gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Block2");
        }        
        EventManager._POINTSTOPED(new EventData(false));
    }
}
