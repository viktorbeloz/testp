﻿using Assets.Scripts.com;
using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Scrollbar _scroll;
    [SerializeField]
    private int _pointScroll;
    [SerializeField]
    private Text _currentScoreText;
    [SerializeField]
    private Slider _progressSlider;
    [SerializeField]
    private Slider _shadowSlider;
    [SerializeField]
    private GameObject _scoreFlag;
    [SerializeField]
    private Text _coinsText;
    [SerializeField]
    private Text _chipText;
    [SerializeField]
    private Text _nameText;

    private User _user;
    private bool _isPressPlay;
    private bool _isStoped;
    private int _currentXP;
    private bool _triger = true;
    private void Start()
    {

        //Subscribing to a Breakpoint Event. Getting with "ItemColController"
        EventManager._POINTSTOPED += GetResultPointerStoped;
        //Subscribe to the event by clicking "Play". Getting with "ButtonController"
        EventManager._PLAY += PlayTap;
        //Subscribing to the event of adding coins. Getting with "ItemMove"
        EventManager._ADDCOINS += AddCoins;
        _user = new User("Kostia", 100, 116, 32852);
        RefreshStats();
    }
    private void GetResultPointerStoped(IData data)
    {
        _isStoped = (bool)((EventData)data).data;
    }
    private void PlayTap(IData data)
    {
        if ((int)_shadowSlider.value == (int)_progressSlider.value)
        {
            _currentXP = (int)_progressSlider.value + 100;
            _shadowSlider.value = _currentXP;
            _scoreFlag.GetComponent<Animation>().Play();
            _isPressPlay = true;
        }
    }
    private void AddCoins(IData data)
    {
        _user.coins += 10;
        RefreshStats();
    }
    //data update
    private void RefreshStats()
    {
        _nameText.text = _user.name;
        _coinsText.text = _user.coins.ToString();
        _chipText.text = _user.chip.ToString();
    }
    //change of progress
    private void FixedUpdate()
    {
        Scroll();
        if (_isPressPlay)
        {            
            if (!_isStoped)
            {
                _progressSlider.value += 1f;
                _currentScoreText.text = ((int)_progressSlider.value).ToString();
                if (_currentXP == (int)_progressSlider.value)
                {
                    _isPressPlay = false;
                }
            }
        }
    }

    private void Scroll()
    {
        if ((int)_progressSlider.value >= _pointScroll && _triger)
        {
            _scroll.value += 1 * Time.deltaTime;
            if ((int)_scroll.value == 1)
            {
                _triger = false;
            }
        }
    }

}
